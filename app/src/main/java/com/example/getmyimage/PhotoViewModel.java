package com.example.getmyimage;

import android.util.Log;
import android.util.Pair;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.getmyimage.api.ApiEndPoint;
import com.example.getmyimage.api.RetrofitManager;
import com.example.getmyimage.data.Photo;
import com.example.getmyimage.data.PhotoResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotoViewModel extends ViewModel {

    private static final String TAG = PhotoViewModel.class.getSimpleName();

    private PhotoResponse mLatestPhotoResponse;

    private MutableLiveData<List<Pair<String,String>>> imageInfoLiveData = new MutableLiveData<List<Pair<String,String>>>();
    private MutableLiveData<Boolean> isNewSearchInProcess = new MutableLiveData<>();

    public boolean isLastPage(){
        if(mLatestPhotoResponse==null || mLatestPhotoResponse.getPhotos() ==null){
            return true;
        }
        return (mLatestPhotoResponse.getPhotos().getPage() ==
                mLatestPhotoResponse.getPhotos().getPages());
    }

    public long getCurrentPage(){
        if(mLatestPhotoResponse == null || mLatestPhotoResponse.getPhotos() ==null){
            return 0;
        }
        return mLatestPhotoResponse.getPhotos().getPage();
    }

    public void refreshPhotoList(String text, long pageNumber, int photoLimitPerPage) {
        if(pageNumber==1){
            isNewSearchInProcess.setValue(true);
        }
        if (text == null) {
            imageInfoLiveData.setValue(null);
            mLatestPhotoResponse = null;
        } else {
            ApiEndPoint apiEndPoint = RetrofitManager.getRetrofitInstance().create(ApiEndPoint.class);

             apiEndPoint.getPhotoResponse(text, pageNumber, photoLimitPerPage).enqueue(new Callback<PhotoResponse>() {
                @Override
                public void onResponse(Call<PhotoResponse> call, Response<PhotoResponse> response) {

                    if(response == null || response.body() == null || response.body().getPhotos() ==null ||
                    response.body().getPhotos().getPhoto() ==null){
                        mLatestPhotoResponse = (response!=null)? response.body(): null;
                        imageInfoLiveData.setValue(null);
                        return;
                    }
                    List<Pair<String, String>> dataset = new ArrayList<>();

                    for(Photo photo: response.body().getPhotos().getPhoto()){
                        String url = String.format("https://farm%s.staticflickr.com/%s/%s_%s_m.jpg", photo.getFarm(),
                                photo.getServer(), photo.getId(), photo.getSecret());
                        String title = photo.getTitle();
                        dataset.add(Pair.create(title, url));
                    }
                    mLatestPhotoResponse = response.body();
                    imageInfoLiveData.setValue(dataset);

                    if(pageNumber==1){
                        isNewSearchInProcess.setValue(false);
                    }
                }

                @Override
                public void onFailure(Call<PhotoResponse> call, Throwable t) {
                    mLatestPhotoResponse = null;
                    imageInfoLiveData.setValue(null);

                    if(pageNumber==1){
                        isNewSearchInProcess.setValue(false);
                    }
                    Log.e(TAG, t.getMessage());
                }
            });
        }
    }

    public MutableLiveData<List<Pair<String,String>>> getImageInfoLiveData() {
        return imageInfoLiveData;
    }

    public MutableLiveData<Boolean> getIsNewSearchInProcess(){
        return isNewSearchInProcess;
    }
}
