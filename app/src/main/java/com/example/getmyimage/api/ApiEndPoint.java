package com.example.getmyimage.api;

import com.example.getmyimage.data.PhotoResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiEndPoint {
    @GET("services/rest/")
    Call<PhotoResponse> getPhotoResponse(@Query("text") String text, @Query("page") long page, @Query("per_page") long perPage);
}