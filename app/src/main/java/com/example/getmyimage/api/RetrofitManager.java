package com.example.getmyimage.api;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitManager {

    private static final String BASE_URL = "https://api.flickr.com";

    private static Retrofit retrofitInstance;

    public static Retrofit getRetrofitInstance(){
        if(retrofitInstance==null){
            synchronized (com.example.getmyimage.api.RetrofitManager.class) {
                if(retrofitInstance==null) {
                    OkHttpClient client = new OkHttpClient.Builder().addInterceptor(
                            new Interceptor() {
                                @Override
                                public Response intercept(Chain chain) throws IOException {
                                    HttpUrl url = chain.request().url().newBuilder().addQueryParameter
                                            ("method", "flickr.photos.search").addQueryParameter
                                            ("api_key", "062a6c0c49e4de1d78497d13a7dbb360").addQueryParameter
                                            ("format", "json").addQueryParameter("nojsoncallback", "!").build();
                                    Response response = chain.proceed(chain.request().newBuilder().url(url).build());
                                    return response;
                                }
                            }
                    ).build();

                    retrofitInstance = new Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .client(client)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                }
            }

        }
        return retrofitInstance;
    }
}
