package com.example.getmyimage;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.getmyimage.data.Photo;
import com.example.getmyimage.data.PhotoResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private  PhotoViewModel mPhotoViewModel;
    private  RecyclerView mRecyclerView;
    private EditText mInputTextView;
    private Button mSearchButton;
    private LinearLayoutManager mLayoutManager;
    private PhotoAdapter mPhotoAdapter;
    private  ProgressBar mProgressBar;
    private Boolean mIsLoading = false;

    private final int ITEM_PER_PAGE = 10;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00FFA4")));

        mRecyclerView = findViewById(R.id.rv_photo_list);
        mInputTextView = findViewById(R.id.et_input_text);
        mSearchButton = findViewById(R.id.btn_search);
        mProgressBar = findViewById(R.id.pb_loader);

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((InputMethodManager)getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
                refreshList(mInputTextView.getText().toString(), 1, ITEM_PER_PAGE);
            }
        });
        mPhotoAdapter = new PhotoAdapter(new ArrayList<>());
        mRecyclerView.setAdapter(mPhotoAdapter);

        mLayoutManager = new LinearLayoutManager(getBaseContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.addOnScrollListener(new PaginationScrollListener(mLayoutManager));
        mPhotoViewModel = new ViewModelProvider(this).get(PhotoViewModel.class);
        subscribePhotoListLiveData();
        subscribeLoadingStatus();
    }


    private void subscribePhotoListLiveData() {
        mPhotoViewModel.getImageInfoLiveData().observe(this, photoListObserver);
    }

    private void subscribeLoadingStatus() {
        mPhotoViewModel.getIsNewSearchInProcess().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean){
                    mRecyclerView.setVisibility(View.GONE);
                    mProgressBar.setVisibility(View.VISIBLE);
                }else{
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });
    }


    Observer<List<Pair<String,String>>> photoListObserver = new Observer<List<Pair<String,String>>>() {
        @Override
        public void onChanged(List<Pair<String,String>> dataset) {
            if(dataset==null || dataset.isEmpty()) {
                mPhotoAdapter.notifyDataChanged(null);
                return;
            }
            if(mPhotoViewModel.getCurrentPage() == 1) {
                mPhotoAdapter.notifyDataChanged(dataset);
            }else{
                mPhotoAdapter.addData(dataset);
            }
            if(!mPhotoViewModel.isLastPage()){
                mPhotoAdapter.addLoader();
            } else{
                mPhotoAdapter.removeLoader();
            }
            mIsLoading = false;
        }
    };

    private void refreshList(String text, long pageNumber, int itemPerPage ) {
        mIsLoading = true;
        mPhotoViewModel.refreshPhotoList(text, pageNumber, itemPerPage);
    }

    private static class PhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<Pair<String, String>> imageInfoDataset;
        private static final int LOADING = 0;
        private static final int ITEM = 1;
        private boolean isLoadingAdded = false;


        public static class PhotoViewHolder extends RecyclerView.ViewHolder {
            private final TextView titleView;
            private final ImageView photoView;


            public PhotoViewHolder(View view) {
                super(view);

                titleView = (TextView) view.findViewById(R.id.tv_title);
                photoView = (ImageView) view.findViewById(R.id.iv_image);
            }

            public TextView getTitleView() {
                return titleView;
            }
            public ImageView getPhotoView() {
                return photoView;
            }
        }

        public static class LoadingViewHolder extends RecyclerView.ViewHolder {
            private ProgressBar progressBar;

            public LoadingViewHolder(View itemView) {
                super(itemView);
                progressBar = (ProgressBar) itemView.findViewById(R.id.pb_loader);

            }
        }


        public PhotoAdapter(List<Pair<String, String>> dataSet) {
            imageInfoDataset = dataSet;
        }


        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder holder = null;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            switch (viewType){
                case ITEM:
                    View view = inflater.inflate(R.layout.row_item, parent, false);
                    holder = new PhotoViewHolder(view);
                    break;
                case LOADING:
                    View viewLoading = inflater.inflate(R.layout.loader_item, parent, false);
                    holder = new LoadingViewHolder(viewLoading);
                    break;
            }
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            switch (getItemViewType(position)) {
                case ITEM:
                    PhotoViewHolder photoViewHolder = (PhotoViewHolder) holder;
                    photoViewHolder.titleView.setText(imageInfoDataset.get(position).first);
                    Picasso.get().load(imageInfoDataset.get(position).second)
                            .placeholder(R.drawable.ic_placeholder_image)
                            .error(R.drawable.ic_error_image)
                            .resize(120, 60)
                            .into(photoViewHolder.photoView);
                    break;

                case LOADING:
                    LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                    loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
                    break;
            }
        }

        @Override
        public int getItemViewType(int position) {
            return (position == imageInfoDataset.size()) ? LOADING: ITEM;
        }

        public void notifyDataChanged(List<Pair<String, String>> dataSet){
            imageInfoDataset = dataSet;
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return (imageInfoDataset==null)? 0 : isLoadingAdded? imageInfoDataset.size()+1 : imageInfoDataset.size();
        }

        public void add(Pair<String, String> data){
            imageInfoDataset.add(data);
            notifyItemInserted(imageInfoDataset.size()-1);
        }
        public void addData(List<Pair<String, String>> dataSet){
            if(getItemViewType(getItemCount()-1) == LOADING){
                notifyItemRemoved(getItemCount()-1);
            }
            if(dataSet==null) return;
            for(Pair<String, String> data: dataSet){
                add(data);
            }
        }

        public void addLoader(){
            isLoadingAdded = true;
            if(getItemViewType(getItemCount()-1) == LOADING){
                return;
            }
            notifyItemInserted(imageInfoDataset.size());
        }

        public void removeLoader(){
            isLoadingAdded = false;
            if(getItemViewType(getItemCount()-1) != LOADING){
                return;
            }
            notifyItemRemoved(imageInfoDataset.size());
        }
    }

    public class PaginationScrollListener extends RecyclerView.OnScrollListener {

        private LinearLayoutManager layoutManager;

        public PaginationScrollListener(LinearLayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (!mIsLoading && !mPhotoViewModel.isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0) {
                    loadMoreItems();
                }
            }
        }

        protected void loadMoreItems(){
            refreshList(mInputTextView.getText().toString(), mPhotoViewModel.getCurrentPage()+1, ITEM_PER_PAGE);

        }
    }
}