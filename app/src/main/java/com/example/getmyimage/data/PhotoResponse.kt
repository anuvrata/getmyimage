package com.example.getmyimage.data

import com.example.getmyimage.data.Photos

data class PhotoResponse (
        val photos: Photos,
        val stats: String
)