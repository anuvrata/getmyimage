package com.example.getmyimage.data

data class Photo (
        val id: String,
        val owner: String,
        val secret: String,
        val server: String,
        val farm: Long,
        val title: String,
        val ispublic: Int,
        val isfamily: Int,
        val isfriend: Int
)
