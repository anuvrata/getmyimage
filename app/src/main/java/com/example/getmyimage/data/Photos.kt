package com.example.getmyimage.data

import com.example.getmyimage.data.Photo

data class Photos(
        val page: Long,
        val pages: Long,
        val perpage: Int,
        val total: Long,
        val photo: List<Photo>
)